package br.edu.uniateneu.subjetiva.ap2.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.edu.uniateneu.subjetiva.ap2.modelo.Aluno;
import br.edu.uniateneu.subjetiva.ap2.modelo.ResponseModel;
import br.edu.uniateneu.subjetiva.ap2.repository.AlunoRepository;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;

@RestController
@RequestMapping(value = "/alunos")
public class AlunoService {
	@Autowired
	AlunoRepository alunoRepository;

	@Produces("application/json")
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody ArrayList<Aluno> retorna() {
		return (ArrayList<Aluno>) alunoRepository.findAll();
	}

	@Consumes("application/json")
	@Produces("application/json")
	@RequestMapping(value = "/salvar", method = RequestMethod.POST)
	public @ResponseBody Aluno salvar(@RequestBody Aluno aluno) {
		Aluno user  = this.alunoRepository.save(aluno);
		return user;
	}

	@Consumes("application/json")
	@Produces("application/json")
	@RequestMapping(value = "/salvar", method = RequestMethod.PUT)
	public @ResponseBody Aluno atualizar(@RequestBody Aluno aluno) {
		Aluno user = this.alunoRepository.save(aluno);
		return user;
	}

	@Produces("application/json")
	@RequestMapping(value = "/deletar/{codigo}", method = RequestMethod.DELETE)
	public @ResponseBody ResponseModel excluir(@PathVariable("codigo") Long codigo) {
		try {
		Aluno aluno = alunoRepository.getReferenceById(codigo);
		System.out.println("********"+ aluno.getId());
		
		
		if (alunoRepository.findById(codigo).isEmpty()) {
			return new ResponseModel(500, "Aluno não encontrado.");
		}
		
			alunoRepository.delete(aluno);
			return new ResponseModel(200, "Registro excluido com sucesso!");

		} catch (Exception e) {
			return new ResponseModel(500, e.getMessage());
		}

	}
	
}

